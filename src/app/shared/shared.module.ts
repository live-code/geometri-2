import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IntToArrayPipe } from './int-to-array.pipe';



@NgModule({
  declarations: [
    IntToArrayPipe
  ],
  exports: [
    IntToArrayPipe
  ],
  imports: [
    CommonModule
  ]
})
export class SharedModule { }
