import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsDemo1Component } from './forms-demo1.component';

const routes: Routes = [{ path: '', component: FormsDemo1Component }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FormsDemo1RoutingModule { }
