import { Component } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, ValidationErrors, Validators} from "@angular/forms";
import {HttpClient} from "@angular/common/http";
import {debounceTime, delay, filter, map} from "rxjs";

type Role = 'admin' | 'moderator';
type FormUser = Partial<User>;

interface User {
  id: number;
  role: Role;
}


@Component({
  selector: 'app-forms-demo1',
  template: `
    <form [formGroup]="form">
      <input type="text" formControlName="name" class="form-control">
      <input type="text" formControlName="surname">
    </form>

    <pre>{{form.get('name')?.errors | json}}</pre>
  `,
})
export class FormsDemo1Component {
  form = new FormGroup({
    name: new FormControl('a', [
      Validators.required,
      Validators.minLength(3),
      // (c: FormControl) => myCustomValidator(c, this.form.get('surname')?.value)
      myCustomValidator
    ]),
    surname: new FormControl(1),
  })
}


function myCustomValidator(c: FormControl): ValidationErrors | null {
  if (c.value.length !== 5) {
    // if errors
    return { not5: true }
  }
  // no errors
  return null
}
