import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsDemo1RoutingModule } from './forms-demo1-routing.module';
import { FormsDemo1Component } from './forms-demo1.component';
import {ReactiveFormsModule} from "@angular/forms";


@NgModule({
  declarations: [
    FormsDemo1Component
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsDemo1RoutingModule
  ]
})
export class FormsDemo1Module { }
