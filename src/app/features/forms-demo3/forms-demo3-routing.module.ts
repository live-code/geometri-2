import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsDemo3Component } from './forms-demo3.component';

const routes: Routes = [{ path: '', component: FormsDemo3Component }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FormsDemo3RoutingModule { }
