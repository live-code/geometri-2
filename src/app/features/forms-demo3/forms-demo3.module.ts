import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsDemo3RoutingModule } from './forms-demo3-routing.module';
import { FormsDemo3Component } from './forms-demo3.component';
import {ReactiveFormsModule} from "@angular/forms";


@NgModule({
  declarations: [
    FormsDemo3Component
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsDemo3RoutingModule
  ]
})
export class FormsDemo3Module { }
