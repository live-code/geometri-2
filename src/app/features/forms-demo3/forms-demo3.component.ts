import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, Validators} from "@angular/forms";
import {subscribeOn} from "rxjs";

@Component({
  selector: 'app-forms-demo3',
  template: `
    <div *ngIf="form.invalid && form.touched">form not valid</div>

    <form [formGroup]="form">
      <input type="checkbox" formControlName="isCompany">

      <input
        type="text"
        formControlName="name"
        placeholder="Name"
        class="form-control"
        [ngClass]="{
          'is-invalid': form.get('name')?.invalid && form.dirty,
          'is-valid': form.get('name')?.valid
        }"
      >

      <input
        type="text"
        formControlName="vat"
        class="form-control"
        [placeholder]="form.get('isCompany')?.value ? 'VAT' : 'CodFisc'"
        [ngClass]="{
            'is-invalid': form.get('vat')?.invalid && form.dirty,
            'is-valid': form.get('vat')?.valid}"
      >
    </form>

    <button [disabled]="form.invalid">SAVE</button>
    <div>VAT_CF valid {{form.get('vat')?.valid}}</div>
  `,
})
export class FormsDemo3Component  {
  form = this.fb.group({
    isCompany: true,
    name: ['', [Validators.required, Validators.minLength(3)]],
    vat: ['']
  })

  constructor(private fb: FormBuilder) {

    this.form.get('isCompany')?.valueChanges
        .subscribe(isCompany => {
          if (isCompany) {
            this.setAsCompany()
          } else {
            this.setAsPerson()
          }
          this.form.get('vat')?.updateValueAndValidity();

        })
    this.setAsCompany();

  }

  setAsPerson() {
    this.form.get('vat')?.setValidators([
      // Validators.required, (c: FormControl) => vatCFValidator(c, 16)
      Validators.required, vatCFValidator(16)
    ])
  }

  setAsCompany() {
    this.form.get('vat')?.setValidators([
      Validators.required, vatCFValidator(11)
    ])
  }
}


export function vatCFValidator(requiredLength: number) {
  return (c: FormControl) => {
    if (c.value && c.value.length !== requiredLength) {
      return { vatCF: true}
    }
    return null
  }
}



/// ========================
// form.validators.ts
const ALPHA_NUMERIC_REGEX = /^\w+$/

export function alphaNumericValidator(c: FormControl) {
  if (c.value && !c.value.match(ALPHA_NUMERIC_REGEX)) {
    return {alphaNumeric: true}
  }
  return null;
}
