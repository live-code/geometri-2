import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsDemo6Component } from './forms-demo6.component';

const routes: Routes = [{ path: '', component: FormsDemo6Component }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FormsDemo6RoutingModule { }
