import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsDemo6RoutingModule } from './forms-demo6-routing.module';
import { FormsDemo6Component } from './forms-demo6.component';
import {ReactiveFormsModule} from "@angular/forms";


@NgModule({
  declarations: [
    FormsDemo6Component
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsDemo6RoutingModule
  ]
})
export class FormsDemo6Module { }
