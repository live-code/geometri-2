import { Component, OnInit } from '@angular/core';
import {AbstractControl, FormArray, FormBuilder, Validators} from "@angular/forms";
interface Product {
  name: string;
  price: string;
}

@Component({
  selector: 'app-forms-demo6',
  template: `
    <h1>Form array</h1>

    <form [formGroup]="form">
      <input formControlName="username">

      <div formArrayName="items">
        <div
          *ngFor="let item of items.controls; let i = index; let last = last"
          [formGroupName]="i"
        >

          <i class="fa fa-check" *ngIf="item.valid"></i>
          <input type="text" placeholder="name" formControlName="name">
          <input type="text" placeholder="price" formControlName="price">
          <i class="fa fa-plus" *ngIf="last && item.valid" (click)="add('', 0)"> </i>
          <i class="fa fa-trash" (click)="remove(i)" *ngIf="items.controls.length > 1"></i>
          <hr>
        </div>
      </div>
    </form>

    <pre>{{form.value | json}}</pre>
  `,
})
export class FormsDemo6Component  {
  form = this.fb.group({
    username: 'abc',
    user: this.fb.group({
      name: '', surname: ''
    }),
    items: this.fb.array<Product>([])
  });
  items = this.form.get('items') as FormArray

  constructor(private fb: FormBuilder) {
    setTimeout(() => {
      const res: any = {
        username: 'pippo',
        user: {
          name: 'Fabio', surname: 'Biondi'
        },
        items: [
          { name: 'Nutella', price: 5},
          { name: 'Milk', price: 2},
        ]
      }

      this.form.patchValue(res)

      for(const item of res.items) {
        this.add(item.name, item.price)
      }
    }, 1000)

    /*this.add('a', 98);
    this.add('a', 99);
    this.add('b', 100);*/
  }

  add(name: string, price: number) {
    this.items.push(
      this.fb.group({
        name: [name, Validators.required],
        price: [price, Validators.required]
      })
    )
  }

  remove(index: number) {
    this.items.removeAt(index)
  }

}
