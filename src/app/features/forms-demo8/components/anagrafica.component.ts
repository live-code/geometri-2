import { Component, OnInit } from '@angular/core';
import {
  AbstractControl,
  ControlValueAccessor,
  FormBuilder, FormControl,
  NG_VALIDATORS,
  NG_VALUE_ACCESSOR, NonNullableFormBuilder, ValidationErrors,
  Validator,
  Validators
} from "@angular/forms";
import {MyFormData} from "../forms-demo8.component";

@Component({
  selector: 'app-anagrafica',
  template: `
    <h1>Anagrafica</h1>
    <form [formGroup]="form">
      <input type="text" formControlName="name">
      <input type="text" formControlName="surname">
    </form>
  `,
  providers: [
    { provide: NG_VALUE_ACCESSOR, useExisting: AnagraficaComponent, multi: true},
    { provide: NG_VALIDATORS, useExisting: AnagraficaComponent, multi: true}
  ]
})
export class AnagraficaComponent implements ControlValueAccessor, Validator {
  form = this.fb.group({
    name: new FormControl('', {nonNullable : true}),
    surname: ['', Validators.required],
  })

  registerOnChange(fn: (val: Partial<MyFormData>) => void): void {
    this.form.valueChanges.subscribe(formData => {
      fn(formData)
    })
  }
  registerOnTouched(fn: any): void {}

  constructor(private fb: NonNullableFormBuilder) {}

  writeValue(obj: Partial<MyFormData>): void {
    this.form.patchValue(obj)
  }

  validate(control: AbstractControl): ValidationErrors | null {
    return this.form.invalid  ? { anagraficaInvalid: true } : null;
  }

}

