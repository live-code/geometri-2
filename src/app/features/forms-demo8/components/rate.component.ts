import { Component, OnInit } from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from "@angular/forms";

@Component({
  selector: 'app-rate',
  template: `
    <p>
      rate works! {{value}}
    </p>
    <i
      class="fa"
      *ngFor="let star of (5 | intToArray); let i = index;"
      [ngClass]="{
         'fa-star': i < value,
         'fa-star-o': i >= value
      }"
      (click)="changeRate(i+1)"
    ></i>
  `,
  providers: [
    { provide: NG_VALUE_ACCESSOR, useExisting: RateComponent, multi: true}
]
})
export class RateComponent implements ControlValueAccessor {
  onChanged!: (val: number) => void;
  onTouched!: () => void;
  value: number = 0;

  registerOnChange(fn: any): void {
    this.onChanged = fn;
  }
  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  writeValue(value: number): void {
    this.value = value;
  }

  changeRate(value: number) {
    this.onChanged(value)
    this.onTouched()
    this.value = value;
  }
}
