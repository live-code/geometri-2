import {Component, Injector, Input, OnInit} from '@angular/core';
import {
  AbstractControl,
  ControlValueAccessor,
  FormControl,
  NG_VALIDATORS,
  NG_VALUE_ACCESSOR, NgControl, ValidationErrors,
  Validator
} from "@angular/forms";

@Component({
  selector: 'app-my-input',
  template: `
    <div class="m-2">
      <div>{{label}}</div>
      <input
        type="text"
        class="form-control"
        [placeholder]="placeholder"
        [formControl]="input"
        (click)="onTouched()"
      >

      <div *ngIf="ngControl.errors?.['required']">required</div>
      <div *ngIf="ngControl.errors?.['minlength']">min x chars</div>
      <div *ngIf="ngControl.errors?.['alphaNumeric']">no symbols</div>

    </div>
  `,
  providers: [
    { provide: NG_VALUE_ACCESSOR, useExisting: MyInputComponent, multi: true},
    { provide: NG_VALIDATORS, useExisting: MyInputComponent, multi: true},
  ]
})
export class MyInputComponent implements ControlValueAccessor, Validator {
  @Input() label: string = '';
  @Input() placeholder: string = '';
  @Input() alphaNumeric: boolean | string  = false;
  ngControl!: NgControl;

  input = new FormControl('', { nonNullable: true });
  onTouched!: () => void;

  constructor(private inj: Injector) {}

  ngOnInit() {
    this.ngControl = this.inj.get(NgControl)
  }

  registerOnChange(fn: any): void {
    this.input.valueChanges.subscribe(fn)
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }
  writeValue(text: string): void {
    this.input.setValue(text)
  }

  validate(control: AbstractControl): ValidationErrors | null {
    if (control.value && this.alphaNumeric && !control.value.match(ALPHA_NUMERIC_REGEX)) {
      return  { alphaNumeric: true }
    }
    return null;
  }

}

const ALPHA_NUMERIC_REGEX = /^([A-Za-z]|[0-9]|_)+$/;
