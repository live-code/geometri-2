import { Component, OnInit } from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from "@angular/forms";

@Component({
  selector: 'app-color-picker',
  template: `
    <div class="d-flex">
      <div
        *ngFor="let color of colors"
        class="cell"
        [ngClass]="{ active: color === activeColor}"
        [style.background-color]="color"
        (click)="changeColor(color)"
        [style.opacity]="isDisabled ? 0.4 : 1"
        [style.pointer-events]="isDisabled ? 'none' : null"
      >
      </div>
    </div>
  `,
  styles: [`
    .cell {width: 30px; height: 30px}
    .active { border: 2px solid black}
  `],
  providers: [
    { provide: NG_VALUE_ACCESSOR, useExisting: ColorPickerComponent, multi: true}
  ]
})
export class ColorPickerComponent implements ControlValueAccessor {
  colors =  [
    '#F44336', '#90CAF9', '#FFCDD2', '#69F0AE', '#AED581', '#FFE082'
  ];

  activeColor: string | undefined;

  isDisabled: boolean = false;
  onChanged!: (newColor: string) => void;
  onTouched!: () => void;

  registerOnChange(fn: any): void {
    this.onChanged = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  writeValue(color: string): void {
    this.activeColor = color;
  }

  setDisabledState?(isDisabled: boolean): void {
    this.isDisabled = isDisabled;
  }
  changeColor(color: string) {
    this.activeColor = color;
    this.onChanged(color)
    this.onTouched();
  }
}
