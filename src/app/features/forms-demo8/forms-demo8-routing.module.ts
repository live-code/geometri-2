import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsDemo8Component } from './forms-demo8.component';

const routes: Routes = [{ path: '', component: FormsDemo8Component }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FormsDemo8RoutingModule { }
