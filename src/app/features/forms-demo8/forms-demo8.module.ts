import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsDemo8RoutingModule } from './forms-demo8-routing.module';
import { FormsDemo8Component } from './forms-demo8.component';
import {ReactiveFormsModule} from "@angular/forms";
import { ColorPickerComponent } from './components/color-picker.component';
import { RateComponent } from './components/rate.component';
import {SharedModule} from "../../shared/shared.module";
import { MyInputComponent } from './components/my-input.component';
import { AnagraficaComponent } from './components/anagrafica.component';


@NgModule({
  declarations: [
    FormsDemo8Component,
    ColorPickerComponent,
    RateComponent,
    MyInputComponent,
    AnagraficaComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsDemo8RoutingModule,
    SharedModule
  ]
})
export class FormsDemo8Module { }
