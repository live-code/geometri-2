import { Component, OnInit } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormControl,
  FormControlState,
  FormGroup, NonNullableFormBuilder,
  ValidatorFn,
  Validators
} from "@angular/forms";

@Component({
  selector: 'app-forms-demo8',
  template: `
    <h1>DEMO CVA</h1>
    <form [formGroup]="form">

      <app-my-input
        [alphaNumeric]="true"
        formControlName="order" label="your order ID" placeholder="write yout name"></app-my-input>

      <app-anagrafica formControlName="anagrafica"></app-anagrafica>

      <app-color-picker formControlName="color"></app-color-picker>
      <app-rate formControlName="rate"></app-rate>

    </form>

    <pre>Valid: {{form.valid}}</pre>
    <pre>{{ form.value | json}}</pre>
  `,
})
export class FormsDemo8Component {
  input2 = new FormControl(123, { nonNullable: true });

  constructor(private fb: NonNullableFormBuilder) {
    this.form.get('color')?.setValue('few')

    setTimeout(() => {
      this.form.get('anagrafica')?.patchValue({ name: 'ewfwe'})
     /* this.form.patchValue({
        anagrafica: {
          name: 'ciccio',
          surname: 'balilla'
        },
        order: 'XYZ',
        rate: 4
      })
      // this.form.patchValue({ color: '#90CAF9'})
     /!* this.form.get('color')?.setValue('#90CAF9')
      this.form.get('color')?.disable()*!/*/
    }, 2000)
  }

  form = this.fb.group({
    order: ['wfew', [Validators.required, Validators.minLength(3)]],
    anagrafica: new FormControl<Partial<MyFormData >>({ name: '', surname: ''}),
    // order: this.fb.nonNullable.control( ['b', [Validators.required, Validators.minLength(3)]]),
    color: ['', Validators.required],
    rate: 2,
  })
}

export interface MyFormData {
  name: string;
  surname: string;
}
