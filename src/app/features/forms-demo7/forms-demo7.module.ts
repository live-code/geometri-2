import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsDemo7RoutingModule } from './forms-demo7-routing.module';
import { FormsDemo7Component } from './forms-demo7.component';


@NgModule({
  declarations: [
    FormsDemo7Component
  ],
  imports: [
    CommonModule,
    FormsDemo7RoutingModule
  ]
})
export class FormsDemo7Module { }
