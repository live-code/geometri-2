import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsDemo7Component } from './forms-demo7.component';

const routes: Routes = [{ path: '', component: FormsDemo7Component }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FormsDemo7RoutingModule { }
