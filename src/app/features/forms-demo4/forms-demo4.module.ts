import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsDemo4RoutingModule } from './forms-demo4-routing.module';
import { FormsDemo4Component } from './forms-demo4.component';
import {ReactiveFormsModule} from "@angular/forms";
import { StepperComponent } from './components/stepper.component';
import { Step1Component } from './components/step1.component';
import { Step2Component } from './components/step2.component';
import { Step3Component } from './components/step3.component';


@NgModule({
  declarations: [
    FormsDemo4Component,
    StepperComponent,
    Step1Component,
    Step2Component,
    Step3Component
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsDemo4RoutingModule
  ]
})
export class FormsDemo4Module { }
