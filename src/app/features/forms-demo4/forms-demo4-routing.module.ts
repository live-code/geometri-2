import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsDemo4Component } from './forms-demo4.component';

const routes: Routes = [{ path: '', component: FormsDemo4Component }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FormsDemo4RoutingModule { }
