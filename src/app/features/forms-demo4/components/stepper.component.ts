import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-stepper',
  template: `
    <div class="card">
      <div class="card-header">{{title}}</div>
      <div class="card-body">
        <ng-content></ng-content>
      </div>
      <div class="card-footer">
        <button *ngIf="isPrevEnabled" (click)="prev.emit()">PREV</button>
        <button *ngIf="isNextEnabled" (click)="next.emit()">NEXT</button>
        <button *ngIf="isSubmitEnabled" (click)="submit.emit()">SUBMIT</button>
      </div>
    </div>
  `,
  styles: [
  ]
})
export class StepperComponent  {
  @Input() title: string = ''
  @Input() isNextEnabled: boolean | undefined= false;
  @Input() isPrevEnabled: boolean = false;
  @Input() isSubmitEnabled: boolean = false;
  @Output() next = new EventEmitter()
  @Output() prev = new EventEmitter()
  @Output() submit = new EventEmitter()
}
