import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-step2',
  template: `
    <div [formGroup]="form">
      <div
        formGroupName="user"
        class="p-3"
        [ngClass]="{'bg-success': form.get('user')?.valid}"
      >
        <h1>User</h1>

        <input
          type="text" formControlName="name"
          class="form-control"
          [ngClass]="{'is-invalid': form.get('user.name')?.invalid}"
        >
        <input type="text" formControlName="surname">
      </div>
    </div>
  `,
  styles: [
  ]
})
export class Step2Component implements OnInit {
  @Input() form: any;

  constructor() { }

  ngOnInit(): void {
  }

}
