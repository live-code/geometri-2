import { Component, OnInit } from '@angular/core';
import {FormBuilder, Validators} from "@angular/forms";

type Step = 'one' | 'two' | 'three';

@Component({
  selector: 'app-forms-demo4',
  template: `
   <form [formGroup]="form" >
     <div [ngSwitch]="step">

       <app-stepper
         title="STEP ONE"
         *ngSwitchCase="'one'"
         [isNextEnabled]="form.get('orderNumber')?.valid"
         (next)="step = 'two'"
       >
         <input type="text" formControlName="orderNumber">
       </app-stepper>


       <app-stepper
         title="STEP TWO"
         *ngSwitchCase="'two'"
         [isPrevEnabled]="true"
         [isNextEnabled]="form.get('user')?.valid"
         (next)="step = 'three'"
         (prev)="step = 'one'"
       >
            <app-step2 [form]="form"></app-step2>

       </app-stepper>

       <app-stepper
         title="STEP 3"
         *ngSwitchCase="'three'"
         [isPrevEnabled]="true"
         [isSubmitEnabled]="true"
         (submit)="saveAll()"
         (prev)="step = 'two'"
       >

         <div
           class="p-3"
           formGroupName="car"
           [ngClass]="{'bg-success': form.get('car')?.valid}"
         >
           <h1>Car</h1>

           <input type="text" formControlName="brand">
           <input type="text" formControlName="name">
         </div>

       </app-stepper>

     <pre>{{form.value | json}}</pre>
   </div>
 </form>
  `,
})
export class FormsDemo4Component {
  step: Step = 'one'

  form  = this.fb.group({
    orderNumber: ['123', Validators.required],
    user: this.fb.group({
      name: ['', Validators.required],
      surname: ['', Validators.required],
    }),
    car: this.fb.group({
      brand: ['', Validators.required],
      name: ['', Validators.required],
    }),
  })

  constructor(private fb: FormBuilder) {
   /* setTimeout(() => {
      const res = {
        orderNumber: '456',
        user: {
          name: 'fab',
          surname: 'bio'
        },
        car:  {
          brand: 'fiat',
          name: '500'
        },
      }

      this.form.setValue(res)
    }, 1000)*/
  }

  saveAll() {
    console.log(this.form.value)
  }
}
