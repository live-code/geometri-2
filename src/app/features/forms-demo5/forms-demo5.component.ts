import {Component, Injectable, OnInit} from '@angular/core';
import {
  AbstractControl,
  AsyncValidatorFn,
  FormBuilder,
  FormControl,
  FormGroup,
  ValidationErrors,
  ValidatorFn,
  Validators
} from "@angular/forms";
import {HttpClient} from "@angular/common/http";
import {debounceTime, map, mergeMap, of, timer} from "rxjs";

@Component({
  selector: 'app-forms-demo5',
  template: `
    <form [formGroup]="form">
      <input type="text" formControlName="username" placeholder="username">


      <div
        formGroupName="passwords"
        style="border: 2px solid black; padding: 10px; margin: 10px"
        [style.border-color]="form.get('passwords')?.valid ? 'green' : 'red'"
      >
        <input type="text" formControlName="password1" placeholder="pass 1'">
        <input type="text" formControlName="password2" placeholder="pass 2'">
      </div>

      <button [disabled]="form.invalid">SAVE</button>
    </form>

    <pre>{{form.valid | json}}</pre>
  `,
  styles: [
  ]
})
export class FormsDemo5Component  {
  form = this.fb.group({
    username: ['', Validators.required, this.userValidator.checkUniqueName() ],
    passwords: this.fb.group(
      {
        password1: [{value: '1234', disabled: false}, [Validators.required, Validators.minLength(3)]],
        password2: ['1234', [Validators.required, Validators.minLength(3)]],
      },
      {
        validators: passwordsMatch()
      }
    )
  })

  constructor(private fb: FormBuilder, private userValidator: UserValidator) {}
}


// ======

function passwordsMatch(): ValidatorFn {
  return (g: AbstractControl): ValidationErrors | null => {
    const p1 = g.get('password1')
    const p2 = g.get('password2')

    if (p1?.value !== p2?.value) {
      return { passwordDoesNotMatch: true}
    }
    return null;
  }
}

// ======
@Injectable({ providedIn: "root"})
export class UserValidator {
  constructor(private http: HttpClient) {
  }

  checkUniqueName(): AsyncValidatorFn  {
    return (control: AbstractControl) => {
      return timer(1000)
        .pipe(
          mergeMap(() =>  this.http.get<any[]>(`https://jsonplaceholder.typicode.com/users?username=${control.value}`)
            .pipe(
              map(res => res.length ? { notAvailable: true} : null)
            ))
        )
    }
  }
}


