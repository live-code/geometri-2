import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsDemo5Component } from './forms-demo5.component';

const routes: Routes = [{ path: '', component: FormsDemo5Component }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FormsDemo5RoutingModule { }
