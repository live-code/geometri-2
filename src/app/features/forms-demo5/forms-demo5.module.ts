import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsDemo5RoutingModule } from './forms-demo5-routing.module';
import { FormsDemo5Component } from './forms-demo5.component';
import {ReactiveFormsModule} from "@angular/forms";


@NgModule({
  declarations: [
    FormsDemo5Component
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsDemo5RoutingModule
  ]
})
export class FormsDemo5Module { }
