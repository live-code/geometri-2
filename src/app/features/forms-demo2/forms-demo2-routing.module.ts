import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsDemo2Component } from './forms-demo2.component';

const routes: Routes = [{ path: '', component: FormsDemo2Component }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FormsDemo2RoutingModule { }
