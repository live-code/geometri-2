import {Component, Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {FormControl} from "@angular/forms";
import {
  catchError,
  debounceTime,
  distinct,
  distinctUntilChanged,
  filter,
  fromEvent,
  interval,
  map,
  mergeAll, mergeMap, Observable, of
} from "rxjs";
import {Meteo, Weather} from "../../model/meteo";

@Component({
  selector: 'app-forms-demo2',
  template: `
    <input
      type="text"
      class="form-control"
      placeholder="Search City"
      [formControl]="input"
    >

    <div *ngIf="meteo$ | async as meteo">
      {{meteo.main.temp}}°
      {{meteo.main.temp}}°
      {{meteo.main.temp}}°
      <img [src]="'http://openweathermap.org/img/w/' + meteo.weather[0].icon + '.png'" alt="">
    </div>

  `,
})
export class FormsDemo2Component  {
  input = new FormControl('', {nonNullable: true});
  meteo$ = this.weatherService.getMeteo(this.input.valueChanges);

  constructor(private weatherService: WeatherService) {}
}





@Injectable({ providedIn: 'root' })
class WeatherService {
  constructor(private http: HttpClient) {}

  getMeteo(input: Observable<string>) {
    return input
      .pipe(
        filter(text => text.length > 3),
        debounceTime(1000),
        distinct(),
        mergeMap(
          txt => this.http.get<Meteo>(`http://api.openweathermap.org/data/2.5/weather?q=${txt}&units=metric&APPID=eb03b1f5e5afb5f4a4edb40c1ef2f534`)
            .pipe(
              catchError(err => {
                return of(null)
              })
            )
        ),
      )
  }
}
