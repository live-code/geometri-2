import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsDemo2RoutingModule } from './forms-demo2-routing.module';
import { FormsDemo2Component } from './forms-demo2.component';
import {ReactiveFormsModule} from "@angular/forms";


@NgModule({
  declarations: [
    FormsDemo2Component
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsDemo2RoutingModule
  ]
})
export class FormsDemo2Module { }
