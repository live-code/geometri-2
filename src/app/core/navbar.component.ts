import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-navbar',
  template: `

    <button routerLink="forms1">Forms 1</button>
    <button routerLink="forms2">Forms 2</button>
    <button routerLink="forms3">Forms 3</button>
    <button routerLink="forms4">Forms 4</button>
    <button routerLink="forms5">Forms 5</button>
    <button routerLink="forms6">Forms 6</button>
    <button routerLink="forms7">Forms 7</button>
    <button routerLink="forms8">Forms 8</button>
  `,
  styles: [
  ]
})
export class NavbarComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
