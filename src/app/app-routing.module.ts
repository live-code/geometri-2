import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: 'forms1', loadChildren: () => import('./features/forms-demo1/forms-demo1.module').then(m => m.FormsDemo1Module) },
  { path: 'forms2', loadChildren: () => import('./features/forms-demo2/forms-demo2.module').then(m => m.FormsDemo2Module) },
  { path: 'forms3', loadChildren: () => import('./features/forms-demo3/forms-demo3.module').then(m => m.FormsDemo3Module) },
  { path: '', redirectTo: 'forms1', pathMatch: 'full' },
  { path: 'forms4', loadChildren: () => import('./features/forms-demo4/forms-demo4.module').then(m => m.FormsDemo4Module) },
  { path: 'forms5', loadChildren: () => import('./features/forms-demo5/forms-demo5.module').then(m => m.FormsDemo5Module) },
  { path: 'forms6', loadChildren: () => import('./features/forms-demo6/forms-demo6.module').then(m => m.FormsDemo6Module) },
  { path: 'forms7', loadChildren: () => import('./features/forms-demo7/forms-demo7.module').then(m => m.FormsDemo7Module) },
  { path: 'forms8', loadChildren: () => import('./features/forms-demo8/forms-demo8.module').then(m => m.FormsDemo8Module) }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
